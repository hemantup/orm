﻿/*
          -- this is sample db table which I am playing around with:
          -- Table: test_table

          -- DROP TABLE test_table;

          CREATE TABLE test_table
          (
            name text,
            id serial NOT NULL,
            CONSTRAINT test_table_pkey PRIMARY KEY (id)
          )
          WITH (
            OIDS=FALSE
          );
          ALTER TABLE test_table
            OWNER TO postgres;

*/
//Install-Package Symbiotic_x64 -Version 4.0.4.1
//Install-Package Symbiotic_Data_Provider_PostgreSql_x64 -Version 2.0.0
//Install-Package Npgsql -Version 3.1.10

//test_table.cs from www.frozenelephant.com/symbiotic/publish.htm  - the Symbiotic Code Generator
//while creating test_table.cs-ORM Data layer using the tool, if you get error like  Could not load file or assembly 'Npgsql....
//you may put the respective version of Npgsql  in the same folder as exe of the Symbiotic Code Generator. Location of exe may be found from Task manager when utility is running. 


//Install-Package CsvHelper -Version 2.16.3
//Install-Package log4net -Version 2.0.7

// set platform target for your project to x64 for this test.

//Uninstall-Package log4net


using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using CsvHelper;
using CsvHelper.Configuration;
using FrozenElephant.Symbiotic;
using Npgsql.Logging;

//using log4net;
//using log4net.Config;

namespace SymbioticDataProviderPostgreSql2
{
    internal class SymbioticDataProviderPostgreSql2Test
    {
        private const string dateTimeFormat = "yyyy-MM-dd-HH-mm-ss-ffff";

        private static void Main(string[] args)
        {
            //XmlConfigurator.Configure();
            //var log = LogManager.GetLogger(typeof(SymbioticDataProviderPostgreSql2Test));
            var numberofrecords = Convert.ToInt32(args[0]);
            var numberofcycles = Convert.ToInt32(args[1]);
            // log.InfoFormat("(SymbioticDataProviderPostgreSql2Test) Number of records per cycle: {0}. Total Number of executioncycles: {1}", numberofrecords, numberofcycles);
            Console.WriteLine(
                "(SymbioticDataProviderPostgreSql2Test) Number of records per cycle: {0}. Total Number of executioncycles: {1}",
                numberofrecords,
                numberofcycles);
            //log.Info("started.");
            Console.WriteLine("started.");
            var createCollection = new List<CsvRow>();
            var retrieveCollection = new List<CsvRow>();
            var updateCollection = new List<CsvRow>();
            var deleteCollection = new List<CsvRow>();
            var stopWatch = new Stopwatch();


            var m_DbTypesFactory = new DatabaseTypesFactoryPostgreSql();
            m_DbTypesFactory.ConnectionString =
                ConfigurationManager.ConnectionStrings["PostgreeDbContext"].ConnectionString;
            var writer = m_DbTypesFactory.CreateObjectWriter();
            var loader = m_DbTypesFactory.CreateObjectLoader();

            //SELECT * FROM pg_stat_activity where application_name = 'SymbioticDataProviderPostgreSql2Test' and datname = 'test_SymbioticDataProviderPostgreSql2' order by query_start desc;
#if DEBUG
            //check output window in Visual Studio
            NpgsqlLogManager.Provider = new ConsoleLoggingProvider();
#endif

            for (var executioncycle = 1; executioncycle <= numberofcycles; executioncycle++)
            {
                #region verify exit

                var getCountQuery = new SqlQuerySimple("SELECT COUNT(*) FROM public.test_table");
                var initialRowCount = loader.ExecuteScalar<int>(m_DbTypesFactory, getCountQuery);
                //in db:
                //SELECT COUNT(*) FROM public.test_table
                if (initialRowCount != 0)
                {
                    //log.InfoFormat("Row count must have been 0 at the beginning of execution cycle {0}",executioncycle);
                    Console.WriteLine("Row count must have been 0 at the beginning of execution cycle {0}",
                        executioncycle);
                    Environment.Exit(-1);
                }

                #endregion

                var instRows = new List<test_table>();
                for (var i = 1; i <= numberofrecords; i++)
                {
                    var tb = new test_table {name = string.Format("{0}_Ex_Cycle:{1}", i, executioncycle)};

                    instRows.Add(tb); //no query fired here
                }
                var rowCreate = new CsvRow();
                //log.InfoFormat("starting create. executioncycle: {0}", executioncycle);
                Console.WriteLine("starting create. executioncycle: {0}", executioncycle);
                rowCreate.Start = DateTime.Now.ToString(dateTimeFormat);
                stopWatch.Start();
                writer.Create(m_DbTypesFactory, instRows, false);
                //in db for each row like:
                //Insert into TEST_TABLE  ( name )  values  ( :name )  returning id
                //name= 2_Ex_Cycle:1 
                // and the a commit
                stopWatch.Stop();
                rowCreate.End = DateTime.Now.ToString(dateTimeFormat);
                //log.InfoFormat("finished creating. executioncycle: {0}", executioncycle);
                Console.WriteLine("finished creating. executioncycle: {0}", executioncycle);
                rowCreate.TimeTaken = stopWatch.Elapsed.TotalMilliseconds.ToString();
                stopWatch.Reset();
                createCollection.Add(rowCreate);
                //var instRows = new List<test_table>();
                //for (var i = 1; i <= numberofrecords; i++)
                //{
                //    var tb = new test_table {name = i.ToString()};

                //    instRows.Add(tb);
                //}
                //  writer.Create(m_DBTypesFactory, instRows,true);
                /*
            System.NotSupportedException was unhandled
  HResult=-2146233067
  Message=Sql server support only!
  Source=FrozenElephant.Symbiotic
  StackTrace:
       at FrozenElephant.Symbiotic.ObjectWriter.Create[T](IDatabaseTypesFactory factory, IList`1 values, Boolean useBulk)
       at SymbioticDataProviderPostgreSql2.SymbioticDataProviderPostgreSql2Test.Main(String[] args) in c:\891\SymbioticDataProviderPostgreSql2\Program.cs:line 49
       at System.AppDomain._nExecuteAssembly(RuntimeAssembly assembly, String[] args)
       at System.AppDomain.ExecuteAssembly(String assemblyFile, Evidence assemblySecurity, String[] args)
       at Microsoft.VisualStudio.HostingProcess.HostProc.RunUsersAssembly()
       at System.Threading.ExecutionContext.RunInternal(ExecutionContext executionContext, ContextCallback callback, Object state, Boolean preserveSyncCtx)
       at System.Threading.ExecutionContext.Run(ExecutionContext executionContext, ContextCallback callback, Object state, Boolean preserveSyncCtx)
       at System.Threading.ExecutionContext.Run(ExecutionContext executionContext, ContextCallback callback, Object state)
       at System.Threading.ThreadHelper.ThreadStart()
  InnerException: 
            */


                var rowRetrieve = new CsvRow();
                // log.InfoFormat("starting retrieve. executioncycle: {0}", executioncycle);
                Console.WriteLine("starting retrieve. executioncycle: {0}", executioncycle);
                rowRetrieve.Start = DateTime.Now.ToString(dateTimeFormat);
                stopWatch.Start();
                var allRows = loader.ObtainItems<test_table>(m_DbTypesFactory,
                    new SqlQuerySimple("SELECT * FROM public.test_table"));
                //in db:
                //SELECT * FROM public.test_table
                stopWatch.Stop();
                rowRetrieve.End = DateTime.Now.ToString(dateTimeFormat);
                //log.InfoFormat("finished retrieve. executioncycle: {0}", executioncycle);
                Console.WriteLine("finished retrieve. executioncycle: {0}", executioncycle);
                rowRetrieve.TimeTaken = stopWatch.Elapsed.TotalMilliseconds.ToString();
                stopWatch.Reset();
                retrieveCollection.Add(rowRetrieve);


                //  log.InfoFormat("\tRetrieved {0} records after inserts.", allrows.Count);
                Console.WriteLine("\tRetrieved {0} records after inserts.", allRows.Count);
                var fstRecord = allRows.OrderBy(x => x.id).First();
                var lastRecord = allRows.OrderBy(x => x.id).Last();
                //  log.InfoFormat("\t\tfstRecord:: {0}||{1}", fstRecord.id,fstRecord.name);
                Console.WriteLine("\t\tfstRecord:: {0}||{1}", fstRecord.id, fstRecord.name);
                //  log.InfoFormat("\t\tlastRecord:: {0}||{1}", lastRecord.id, lastRecord.name);
                Console.WriteLine("\t\tlastRecord:: {0}||{1}", lastRecord.id, lastRecord.name);


                var rowUpdate = new CsvRow();
                //log.InfoFormat("starting update. executioncycle: {0}", executioncycle);
                Console.WriteLine("starting update. executioncycle: {0}", executioncycle);
                rowUpdate.Start = DateTime.Now.ToString(dateTimeFormat);
                foreach (var item in allRows)
                {
                    item.name = string.Format("{0}_{1}", item.name, DateTime.Now.ToString(dateTimeFormat));
                    stopWatch.Start();
                    writer.Update(m_DbTypesFactory, item);
                    //in db:
                    //for each row separate query like Update test_table Set name = :name where id = :id
                    //name= 1_Ex_Cycle:1_2017-03-18-22-06-31-7881 | id= 4 
                    //commit after each row
                    stopWatch.Stop();
                }
                rowUpdate.End = DateTime.Now.ToString(dateTimeFormat);
                //log.InfoFormat("finished update. executioncycle: {0}", executioncycle);
                Console.WriteLine("finished update. executioncycle: {0}", executioncycle);
                rowUpdate.TimeTaken = stopWatch.Elapsed.TotalMilliseconds.ToString();
                stopWatch.Reset();
                updateCollection.Add(rowUpdate);


                var allrowsAgain = loader.ObtainItems<test_table>(m_DbTypesFactory,
                    new SqlQuerySimple("SELECT * FROM public.test_table"));
                //in db:
                //SELECT * FROM public.test_table

                //  log.InfoFormat("\tRetrieved {0} records after updates.", allrowsAgain.Count);
                Console.WriteLine("\tRetrieved {0} records after updates.", allrowsAgain.Count);
                var fstRecordAgain = allrowsAgain.OrderBy(x => x.id).First();
                var lastRecordAgain = allrowsAgain.OrderBy(x => x.id).Last();
                //  log.InfoFormat("\t\tfstRecord:: {0}||{1}", fstRecordAgain.id, fstRecordAgain.name);
                Console.WriteLine("\t\tfstRecord:: {0}||{1}", fstRecordAgain.id, fstRecordAgain.name);
                //  log.InfoFormat("\t\tlastRecord:: {0}||{1}", lastRecordAgain.id, lastRecordAgain.name);
                Console.WriteLine("\t\tlastRecord:: {0}||{1}", lastRecordAgain.id, lastRecordAgain.name);


                var rowDelete = new CsvRow();
                //log.InfoFormat("starting delete. executioncycle: {0}", executioncycle);
                Console.WriteLine("starting delete. executioncycle: {0}", executioncycle);
                rowDelete.Start = DateTime.Now.ToString(dateTimeFormat);
                foreach (var item in allrowsAgain)
                {
                    stopWatch.Start();
                    writer.Delete(m_DbTypesFactory, item);
                    //in db:
                    // for each row query like  Delete from test_table where id = :id
                    //id= 4 
                    //and commit after each row
                    stopWatch.Stop();
                }
                rowDelete.End = DateTime.Now.ToString(dateTimeFormat);
                // log.InfoFormat("finished delete. executioncycle: {0}", executioncycle);
                Console.WriteLine("finished delete. executioncycle: {0}", executioncycle);
                rowDelete.TimeTaken = stopWatch.Elapsed.TotalMilliseconds.ToString();
                stopWatch.Reset();
                deleteCollection.Add(rowDelete);

                /*
                writer.ExecuteNonQuery(m_DbTypesFactory,
                    new SqlQuerySimple("ALTER SEQUENCE test_table_id_seq RESTART WITH 1"));
                 
                FrozenElephant.Symbiotic.SqlInjectionValidationException was unhandled
  HResult=-2146233088
  Message=Found alter commands in the sql statement.
  Source=FrozenElephant.Symbiotic
  StackTrace:
       at FrozenElephant.Symbiotic.SqlQuerySimple.ValidateNoAlter()
       at FrozenElephant.Symbiotic.SqlQuerySimple.ValidateSql()
       at FrozenElephant.Symbiotic.SqlQuerySimple.ObtainSql()
       at FrozenElephant.Symbiotic.ObjectWriter.ExecuteNonQuery(IDatabaseTypesFactory factory, ISqlQuery sql)
       at SymbioticDataProviderPostgreSql2.SymbioticDataProviderPostgreSql2Test.Main(String[] args) in c:\891\SymbioticDataProviderPostgreSql2\Program.cs:line 175
       at System.AppDomain._nExecuteAssembly(RuntimeAssembly assembly, String[] args)
       at System.AppDomain.ExecuteAssembly(String assemblyFile, Evidence assemblySecurity, String[] args)
       at Microsoft.VisualStudio.HostingProcess.HostProc.RunUsersAssembly()
       at System.Threading.ExecutionContext.RunInternal(ExecutionContext executionContext, ContextCallback callback, Object state, Boolean preserveSyncCtx)
       at System.Threading.ExecutionContext.Run(ExecutionContext executionContext, ContextCallback callback, Object state, Boolean preserveSyncCtx)
       at System.Threading.ExecutionContext.Run(ExecutionContext executionContext, ContextCallback callback, Object state)
       at System.Threading.ThreadHelper.ThreadStart()
  InnerException: 
                */

                //   ResetSequence();

                //fixed Found alter commands in the sql statement. by ShouldPerformValidation. No need for ResetSequence by direct Npgsql now
                var alterSeqQ = new SqlQuerySimple("ALTER SEQUENCE test_table_id_seq RESTART WITH 1");
                alterSeqQ.ShouldPerformValidation = false;
                writer.ExecuteNonQuery(m_DbTypesFactory, alterSeqQ);
                //in db:
                //ALTER SEQUENCE test_table_id_seq RESTART WITH 1
            }
            CreateCSVs(createCollection, retrieveCollection, updateCollection, deleteCollection);

            Console.WriteLine("Done");
            // Console.ReadLine();
        }

        //private static void ResetSequence()
        //{
        //    var cString = ConfigurationManager.ConnectionStrings["PostgreeDbContext"].ConnectionString;
        //    var conn = new NpgsqlConnection(cString);
        //    conn.Open();
        //    new NpgsqlCommand("ALTER SEQUENCE test_table_id_seq RESTART WITH 1;", conn).ExecuteNonQuery();
        //    conn.Close();
        //}

        private static void CreateCSVs(List<CsvRow> createCollection, List<CsvRow> retrieveCollection,
            List<CsvRow> updateCollection,
            List<CsvRow> deleteCollection)
        {
            using (var sw = new StreamWriter(ConfigurationManager.AppSettings["csvLocationcreate"]))
            {
                var csv = new CsvWriter(sw);
                csv.Configuration.RegisterClassMap<CsvRowMap>();
                csv.WriteRecords(createCollection);
            }
            using (var sw = new StreamWriter(ConfigurationManager.AppSettings["csvLocationretrieve"]))
            {
                var csv = new CsvWriter(sw);

                csv.WriteRecords(retrieveCollection);
            }
            using (var sw = new StreamWriter(ConfigurationManager.AppSettings["csvLocationupdate"]))
            {
                var csv = new CsvWriter(sw);

                csv.WriteRecords(updateCollection);
            }
            using (var sw = new StreamWriter(ConfigurationManager.AppSettings["csvLocationdelete"]))
            {
                var csv = new CsvWriter(sw);

                csv.WriteRecords(deleteCollection);
            }
        }
    }

    public class CsvRow
    {
        public string Start { get; set; }
        public string End { get; set; }
        public string TimeTaken { get; set; }
    }


    public sealed class CsvRowMap : CsvClassMap<CsvRow>
    {
        public CsvRowMap()
        {
            AutoMap();
        }
    }
}

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using CsvHelper;
using CsvHelper.Configuration;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Dialect;
using NHibernate.Driver;
using NHibernate.Linq;
using Configuration = NHibernate.Cfg.Configuration;
using Environment = System.Environment;

//using log4net;
//using log4net.Config;

namespace NHibernate4
{
    internal class NHibernate4Test
    {
        private const string dateTimeFormat = "yyyy-MM-dd-HH-mm-ss-ffff";

        private static void Main(string[] args)
        {
            /*
          -- this is sample db table which you are playing around with:
          -- Table: test_table

          -- DROP TABLE test_table;

          CREATE TABLE test_table
          (
            name text,
            id serial NOT NULL,
            CONSTRAINT test_table_pkey PRIMARY KEY (id)
          )
          WITH (
            OIDS=FALSE
          );
          ALTER TABLE test_table
            OWNER TO postgres;

          */
            //Install-Package NHibernate -Version 4.1.1.4000
            //strange but I had to install Npgsql seprately, it should have been added as dependency to NHibernate at first place if needed. 
            //Install-Package Npgsql -Version 3.1.10
            //  removed Install-Package log4net -Version 2.0.7
            //Uninstall-Package log4net
            //Install-Package CsvHelper -Version 2.16.3


            //XmlConfigurator.Configure();
            //var log = LogManager.GetLogger(typeof(NHibernate4Test));
            var numberofrecords = Convert.ToInt32(args[0]);
            var numberofcycles = Convert.ToInt32(args[1]);
            // log.InfoFormat("(NHibernate4Test) Number of records per cycle: {0}. Total Number of executioncycles: {1}", numberofrecords, numberofcycles);
            Console.WriteLine(
                "(NHibernate4Test) Number of records per cycle: {0}. Total Number of executioncycles: {1}",
                numberofrecords,
                numberofcycles);
            //log.Info("started.");
            Console.WriteLine("started.");
            var createCollection = new List<CsvRow>();
            var retrieveCollection = new List<CsvRow>();
            var updateCollection = new List<CsvRow>();
            var deleteCollection = new List<CsvRow>();
            var stopWatch = new Stopwatch();

            var sessionFactory = SessionFactory();
            //    SELECT * FROM pg_stat_activity where application_name = 'NHibernate4Test' and datname = 'test_NHibernate'  order by query_start desc;
            // see application name and database name in app config
            //or use setings.LogSqlInConsole = true;
            // but on var sessionFactory = SessionFactory(); a query was fired to db which is not recorded by LogSqlInConsole:
            /*
             SELECT ns.nspname, a.typname, a.oid, a.typrelid, a.typbasetype,
CASE WHEN pg_proc.proname='array_recv' THEN 'a' ELSE a.typtype END AS type,
CASE
  WHEN pg_proc.proname='array_recv' THEN a.typelem
  WHEN a.typtype='r' THEN rngsubtype
  ELSE 0
END AS elemoid,
CASE
  WHEN pg_proc.proname IN ('array_recv','oidvectorrecv') THEN 3    -- Arrays last 
  WHEN a.typtype='r' THEN 2                                       -- Ranges before 
  WHEN a.typtype='d' THEN 1                                        -- Domains before
  ELSE 0                                                         -- Base types first 
END AS ord
FROM pg_type AS a
JOIN pg_namespace AS ns ON (ns.oid = a.typnamespace)
JOIN pg_proc ON pg_proc.oid = a.typreceive
LEFT OUTER JOIN pg_type AS b ON (b.oid = a.typelem)
LEFT OUTER JOIN pg_range ON (pg_range.rngtypid = a.oid) 
WHERE
  (
    a.typtype IN ('b', 'r', 'e', 'd') AND
    (b.typtype IS NULL OR b.typtype IN ('b', 'r', 'e', 'd'))  -- Either non-array or array of supported element type 
  )
             */
            for (var executioncycle = 1; executioncycle <= numberofcycles; executioncycle++)
            {
                #region verify exit

                var initialRowCount = 0;
                using (var ses = sessionFactory.OpenSession())
                {
                    initialRowCount = ses.Query<TestTableRow>().Count();
                    //query in db:
                    //select cast(count(*) as int4) as col_0_0_ from test_table testtabler0_
                }
                if (initialRowCount != 0)
                {
                    //log.InfoFormat("Row count must have been 0 at the beginning of execution cycle {0}",executioncycle);
                    Console.WriteLine("Row count must have been 0 at the beginning of execution cycle {0}",
                        executioncycle);
                    Environment.Exit(-1);
                }

                #endregion

                var rowCreate = new CsvRow();
                using (var ses = sessionFactory.OpenSession())
                {
                    using (var tarns = ses.BeginTransaction())
                    {
                        //log.InfoFormat("starting create. executioncycle: {0}", executioncycle);
                        Console.WriteLine("starting create. executioncycle: {0}", executioncycle);
                        rowCreate.Start = DateTime.Now.ToString(dateTimeFormat);
                        for (var i = 1; i <= numberofrecords; i++)
                        {
                            var tb = new TestTableRow {Name = string.Format("{0}_Ex_Cycle:{1}", i, executioncycle)};
                            //on each save below, since sequence is used, 
                            //select nextval ('test_table_id_seq')
                            //is fired.( Revist TestTableRow.hbm.xml)
                            // so this time should also be counted here while creation.
                            stopWatch.Start();
                            ses.Save(tb);
                            // eventhough this save doesn't fire query to insert record, but retrives sequence, so counting in this overhead.
                            stopWatch.Stop();
                        }
                        stopWatch.Start();
                        tarns.Commit(); // caution this transaction will not be able to control rollback for sequnces.
                        //indb:
                        //numberofrecords count querieis like:
                        //INSERT INTO test_table (name, id) VALUES (:p0, :p1);:p0 = '5000_Ex_Cycle:1' [Type: String (0:0:0)], :p1 = 5000 [Type: Int32 (0:0:0)]
                        //and then commit
                        stopWatch.Stop();
                        rowCreate.End = DateTime.Now.ToString(dateTimeFormat);
                        //log.InfoFormat("finished creating. executioncycle: {0}", executioncycle);
                        Console.WriteLine("finished creating. executioncycle: {0}", executioncycle);
                        rowCreate.TimeTaken = stopWatch.Elapsed.TotalMilliseconds.ToString();
                    }
                }
                stopWatch.Reset();
                createCollection.Add(rowCreate);


                var rowRetrieve = new CsvRow();
                var rowUpdate = new CsvRow();
                using (var ses = sessionFactory.OpenSession())
                {
                    using (var tarns = ses.BeginTransaction())
                    {
                        //List<TestTableRow> allrows = ses.Query<TestTableRow>().Where(x => x.Name == "aaa").ToList();


                        // log.InfoFormat("starting retrieve. executioncycle: {0}", executioncycle);
                        Console.WriteLine("starting retrieve. executioncycle: {0}", executioncycle);
                        rowRetrieve.Start = DateTime.Now.ToString(dateTimeFormat);
                        stopWatch.Start();
                        var allrows = ses.Query<TestTableRow>().ToList(); // actual query fired on ToList()
                        stopWatch.Stop();
                        rowRetrieve.End = DateTime.Now.ToString(dateTimeFormat);
                        //log.InfoFormat("finished retrieve. executioncycle: {0}", executioncycle);
                        Console.WriteLine("finished retrieve. executioncycle: {0}", executioncycle);
                        rowRetrieve.TimeTaken = stopWatch.Elapsed.TotalMilliseconds.ToString();
                        stopWatch.Reset();


                        //  log.InfoFormat("\tRetrieved {0} records after inserts.", allrows.Count);
                        Console.WriteLine("\tRetrieved {0} records after inserts.", allrows.Count);
                        var fstRecord = allrows.OrderBy(x => x.Id).First();
                        var lastRecord = allrows.OrderBy(x => x.Id).Last();
                        //  log.InfoFormat("\t\tfstRecord:: {0}||{1}", fstRecord.Id,fstRecord.Name);
                        Console.WriteLine("\t\tfstRecord:: {0}||{1}", fstRecord.Id, fstRecord.Name);
                        //  log.InfoFormat("\t\tlastRecord:: {0}||{1}", lastRecord.Id, lastRecord.Name);
                        Console.WriteLine("\t\tlastRecord:: {0}||{1}", lastRecord.Id, lastRecord.Name);


                        foreach (var item in allrows)
                        {
                            item.Name = string.Format("{0}_{1}", item.Name, DateTime.Now.ToString(dateTimeFormat));
                            ses.Update(item); // no query fired at this time, but commited on trans commit
                        }
                        //log.InfoFormat("starting update. executioncycle: {0}", executioncycle);
                        Console.WriteLine("starting update. executioncycle: {0}", executioncycle);
                        rowUpdate.Start = DateTime.Now.ToString(dateTimeFormat);
                        stopWatch.Start();
                        tarns.Commit();
                        //sends command to update both id and name ( single query per update). It should have sent query to update only name when name only is changed. Might be done to keep objects in sync.
                        //indb :
                        //allrows.count number of update queries like:
                        //UPDATE test_table SET name = :p0 WHERE id = :p1;:p0 = '5000_Ex_Cycle:1_2017-03-18-13-33-36-2182' [Type: String (0:0:0)], :p1 = 5000 [Type: Int32 (0:0:0)]
                        // and then finally commit
                        stopWatch.Stop();
                        rowUpdate.End = DateTime.Now.ToString(dateTimeFormat);
                        //log.InfoFormat("finished update. executioncycle: {0}", executioncycle);
                        Console.WriteLine("finished update. executioncycle: {0}", executioncycle);
                        rowUpdate.TimeTaken = stopWatch.Elapsed.TotalMilliseconds.ToString();
                        stopWatch.Reset();
                    }
                }
                retrieveCollection.Add(rowRetrieve);
                updateCollection.Add(rowUpdate);


                var rowDelete = new CsvRow();
                using (var ses = sessionFactory.OpenSession())
                {
                    using (var tarns = ses.BeginTransaction())
                    {
                        var allrowsAgain = ses.Query<TestTableRow>().ToList();
                        //already recorded retrive time in update above, so skipping here
                        //in db:
                        //select testtabler0_.id as id1_0_, testtabler0_.name as name2_0_ from test_table testtabler0_


                        //  log.InfoFormat("\tRetrieved {0} records after updates.", allrowsAgain.Count);
                        Console.WriteLine("\tRetrieved {0} records after updates.", allrowsAgain.Count);
                        var fstRecord = allrowsAgain.OrderBy(x => x.Id).First();
                        var lastRecord = allrowsAgain.OrderBy(x => x.Id).Last();
                        //  log.InfoFormat("\t\tfstRecord:: {0}||{1}", fstRecord.Id,fstRecord.Name);
                        Console.WriteLine("\t\tfstRecord:: {0}||{1}", fstRecord.Id, fstRecord.Name);
                        //  log.InfoFormat("\t\tlastRecord:: {0}||{1}", lastRecord.Id, lastRecord.Name);
                        Console.WriteLine("\t\tlastRecord:: {0}||{1}", lastRecord.Id, lastRecord.Name);


                        foreach (var item in allrowsAgain)
                        {
                            ses.Delete(item); // no query fired at this time, but commited on trans commit
                        }
                        //log.InfoFormat("starting delete. executioncycle: {0}", executioncycle);
                        Console.WriteLine("starting delete. executioncycle: {0}", executioncycle);
                        rowDelete.Start = DateTime.Now.ToString(dateTimeFormat);
                        stopWatch.Start();
                        tarns.Commit();
                        //indb:
                        // allrowsAgain.count number of delete queries like:
                        //DELETE FROM test_table WHERE id = :p0;:p0 = 5000 [Type: Int32 (0:0:0)]
                        // and then finally commit
                        stopWatch.Stop();
                        rowDelete.End = DateTime.Now.ToString(dateTimeFormat);
                        //log.InfoFormat("finished delete. executioncycle: {0}", executioncycle);
                        Console.WriteLine("finished delete. executioncycle: {0}", executioncycle);
                        rowDelete.TimeTaken = stopWatch.Elapsed.TotalMilliseconds.ToString();
                        stopWatch.Reset();
                    }
                }
                deleteCollection.Add(rowDelete);

                using (var ses = sessionFactory.OpenSession())
                {
                    ses.CreateSQLQuery("ALTER SEQUENCE test_table_id_seq RESTART WITH 1;").ExecuteUpdate();
                    //in db:
                    //ALTER SEQUENCE test_table_id_seq RESTART WITH 1
                }
            }

            CreateCSVs(createCollection, retrieveCollection, updateCollection, deleteCollection);

            Console.WriteLine("Done");
            //  Console.ReadLine();
        }

        //private static void ResetSequence()
        //{
        //    var cString = ConfigurationManager.ConnectionStrings["PostgreeDbContext"].ConnectionString;
        //    var conn = new NpgsqlConnection(cString);
        //    conn.Open();
        //    new NpgsqlCommand("ALTER SEQUENCE test_table_id_seq RESTART WITH 1;", conn).ExecuteNonQuery();
        //    conn.Close();
        //}


        private static void CreateCSVs(List<CsvRow> createCollection, List<CsvRow> retrieveCollection,
            List<CsvRow> updateCollection,
            List<CsvRow> deleteCollection)
        {
            using (var sw = new StreamWriter(ConfigurationManager.AppSettings["csvLocationcreate"]))
            {
                var csv = new CsvWriter(sw);
                csv.Configuration.RegisterClassMap<CsvRowMap>();
                csv.WriteRecords(createCollection);
            }
            using (var sw = new StreamWriter(ConfigurationManager.AppSettings["csvLocationretrieve"]))
            {
                var csv = new CsvWriter(sw);

                csv.WriteRecords(retrieveCollection);
            }
            using (var sw = new StreamWriter(ConfigurationManager.AppSettings["csvLocationupdate"]))
            {
                var csv = new CsvWriter(sw);

                csv.WriteRecords(updateCollection);
            }
            using (var sw = new StreamWriter(ConfigurationManager.AppSettings["csvLocationdelete"]))
            {
                var csv = new CsvWriter(sw);

                csv.WriteRecords(deleteCollection);
            }
        }

        private static ISessionFactory SessionFactory()
        {
            var Config = new Configuration();
            Config.DataBaseIntegration(setings =>
            {
                setings.ConnectionStringName = "PostgreeDbContext";
                // becareful its ConnectionStringName I am giving , not the full ConnectionString
                setings.Driver<NpgsqlDriver>();
                setings.Dialect<PostgreSQL82Dialect>(); // mine is PostgreSQL 9.6.1/9.5.5
#if DEBUG
                setings.LogSqlInConsole = true;
#endif
            });
            Config.AddAssembly(Assembly.GetExecutingAssembly());


            var sessionFactory = Config.BuildSessionFactory();
            return sessionFactory;
        }
    }

    // mapping embedded in dll via TestTableRow.hbm.xml
    public class TestTableRow
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
    }

    public class CsvRow
    {
        public string Start { get; set; }
        public string End { get; set; }
        public string TimeTaken { get; set; }
    }


    public sealed class CsvRowMap : CsvClassMap<CsvRow>
    {
        public CsvRowMap()
        {
            AutoMap();
        }
    }
}


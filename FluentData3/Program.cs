﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using CsvHelper;
using CsvHelper.Configuration;
using FluentData;

//using log4net;
//using log4net.Config;

namespace FluentData3
{
    internal class FluentData3Test
    {
        private const string dateTimeFormat = "yyyy-MM-dd-HH-mm-ss-ffff";
        /*
        -- this is sample db table which you are playing around with:
        -- Table: test_table

        -- DROP TABLE test_table;

        CREATE TABLE test_table
        (
          name text,
          id serial NOT NULL,
          CONSTRAINT test_table_pkey PRIMARY KEY (id)
        )
        WITH (
          OIDS=FALSE
        );
        ALTER TABLE test_table
          OWNER TO postgres;

        */
        //Install-Package Npgsql -Version 3.1.10
        //  removed Install-Package log4net -Version 2.0.7
        //Uninstall-Package log4net
        //Install-Package CsvHelper -Version 2.16.3
        //Install-Package FluentData -Version 3.0.1

        private static void Main(string[] args)
        {
            //XmlConfigurator.Configure();
            //var log = LogManager.GetLogger(typeof(FluentData3Test));
            var numberofrecords = Convert.ToInt32(args[0]);
            var numberofcycles = Convert.ToInt32(args[1]);
            // log.InfoFormat("(FluentData3Test) Number of records per cycle: {0}. Total Number of executioncycles: {1}", numberofrecords, numberofcycles);
            Console.WriteLine(
                "(FluentData3Test) Number of records per cycle: {0}. Total Number of executioncycles: {1}",
                numberofrecords,
                numberofcycles);
            //log.Info("started.");
            Console.WriteLine("started.");
            var createCollection = new List<CsvRow>();
            var retrieveCollection = new List<CsvRow>();
            var updateCollection = new List<CsvRow>();
            var deleteCollection = new List<CsvRow>();
            var stopWatch = new Stopwatch();


            for (var executioncycle = 1; executioncycle <= numberofcycles; executioncycle++)
            {
                #region verify exit

                using (var ctx = new DbContext().ConnectionStringName("PostgreeDbContext", new PostgreSqlProvider())
                    )
                {
                    var initialRowCount = 0;
                    initialRowCount = ctx.Sql(@"SELECT COUNT(*)
			FROM public.test_table").QuerySingle<int>();

                    if (initialRowCount != 0)
                    {
                        //log.InfoFormat("Row count must have been 0 at the beginning of execution cycle {0}",executioncycle);
                        Console.WriteLine("Row count must have been 0 at the beginning of execution cycle {0}",
                            executioncycle);
                        Environment.Exit(-1);
                    }
                }

                #endregion

                using (var ctx = new DbContext().ConnectionStringName("PostgreeDbContext", new PostgreSqlProvider())
                    )
                {
                    var rowCreate = new CsvRow();
                    var query = new StringBuilder();
                    for (var i = 1; i <= numberofrecords; i++)
                    {
                        query.AppendFormat("INSERT INTO public.test_table(name, id ) VALUES ('{0}', DEFAULT); ", string.Format("{0}_Ex_Cycle:{1}", i, executioncycle));
                    }
                    //log.InfoFormat("starting create. executioncycle: {0}", executioncycle);
                    Console.WriteLine("starting create. executioncycle: {0}", executioncycle);
                    rowCreate.Start = DateTime.Now.ToString(dateTimeFormat);
                    stopWatch.Start();
                    ctx.Sql(query.ToString()).Execute();
                    stopWatch.Stop();
                    rowCreate.End = DateTime.Now.ToString(dateTimeFormat);
                    //log.InfoFormat("finished creating. executioncycle: {0}", executioncycle);
                    Console.WriteLine("finished creating. executioncycle: {0}", executioncycle);
                    rowCreate.TimeTaken = stopWatch.Elapsed.TotalMilliseconds.ToString();
                    stopWatch.Reset();
                    createCollection.Add(rowCreate);
                }

                using (var ctx = new DbContext().ConnectionStringName("PostgreeDbContext", new PostgreSqlProvider())
                    )
                {
                    var rowRetrieve = new CsvRow();
                    //log.InfoFormat("starting retrieve. executioncycle: {0}", executioncycle);
                    Console.WriteLine("starting retrieve. executioncycle: {0}", executioncycle);
                    rowRetrieve.Start = DateTime.Now.ToString(dateTimeFormat);
                    stopWatch.Start();
                    var allrows =
                        ctx.Sql("SELECT id \"Id\", name  \"Name\" FROM public.test_table; ")
                            .QueryMany<TestTableRow>(Custom_mapper_using_datareader);
                    stopWatch.Stop();
                    rowRetrieve.End = DateTime.Now.ToString(dateTimeFormat);
                    //log.InfoFormat("finished retrieve. executioncycle: {0}", executioncycle);
                    Console.WriteLine("finished retrieve. executioncycle: {0}", executioncycle);
                    rowRetrieve.TimeTaken = stopWatch.Elapsed.TotalMilliseconds.ToString();
                    stopWatch.Reset();
                    retrieveCollection.Add(rowRetrieve);


                    //  log.InfoFormat("\tRetrieved {0} records after inserts.", allrows.Count);
                    Console.WriteLine("\tRetrieved {0} records after inserts.", allrows.Count);
                    var fstRecord = allrows.OrderBy(x => x.Id).First();
                    var lastRecord = allrows.OrderBy(x => x.Id).Last();
                    //  log.InfoFormat("\t\tfstRecord:: {0}||{1}", fstRecord.Id,fstRecord.Name);
                    Console.WriteLine("\t\tfstRecord:: {0}||{1}", fstRecord.Id, fstRecord.Name);
                    //  log.InfoFormat("\t\tlastRecord:: {0}||{1}", lastRecord.Id, lastRecord.Name);
                    Console.WriteLine("\t\tlastRecord:: {0}||{1}", lastRecord.Id, lastRecord.Name);

                    var updateQuery = new StringBuilder();
                    foreach (var item in allrows)
                    {
                        updateQuery.AppendFormat("UPDATE public.test_table SET name = '{0}_{1}' WHERE id = {2}; ",
                            item.Name, DateTime.Now.ToString(dateTimeFormat), item.Id);
                    }


                    var rowUpdate = new CsvRow();
                    //log.InfoFormat("starting update. executioncycle: {0}", executioncycle);
                    Console.WriteLine("starting update. executioncycle: {0}", executioncycle);
                    rowUpdate.Start = DateTime.Now.ToString(dateTimeFormat);
                    stopWatch.Start();
                    ctx.Sql(updateQuery.ToString()).Execute();
                    stopWatch.Stop();
                    rowUpdate.End = DateTime.Now.ToString(dateTimeFormat);
                    //log.InfoFormat("finished update. executioncycle: {0}", executioncycle);
                    Console.WriteLine("finished update. executioncycle: {0}", executioncycle);
                    rowUpdate.TimeTaken = stopWatch.Elapsed.TotalMilliseconds.ToString();
                    stopWatch.Reset();
                    updateCollection.Add(rowUpdate);
                }
                using (var ctx = new DbContext().ConnectionStringName("PostgreeDbContext", new PostgreSqlProvider())
                    )
                {
                    var allrowsAgain =
                        ctx.Sql("SELECT id \"Id\", name  \"Name\" FROM public.test_table")
                            .QueryMany<TestTableRow>(Custom_mapper_using_datareader);

                    //  log.InfoFormat("\tRetrieved {0} records after updates.", allrowsAgain.Count);
                    Console.WriteLine("\tRetrieved {0} records after updates.", allrowsAgain.Count);
                    var fstRecord = allrowsAgain.OrderBy(x => x.Id).First();
                    var lastRecord = allrowsAgain.OrderBy(x => x.Id).Last();
                    //  log.InfoFormat("\t\tfstRecord:: {0}||{1}", fstRecord.Id,fstRecord.Name);
                    Console.WriteLine("\t\tfstRecord:: {0}||{1}", fstRecord.Id, fstRecord.Name);
                    //  log.InfoFormat("\t\tlastRecord:: {0}||{1}", lastRecord.Id, lastRecord.Name);
                    Console.WriteLine("\t\tlastRecord:: {0}||{1}", lastRecord.Id, lastRecord.Name);
                    var delQuery = new StringBuilder();
                    foreach (var item in allrowsAgain)
                    {
                        delQuery.AppendFormat("DELETE FROM public.test_table WHERE id = {0}; ", item.Id);
                    }


                    var rowDelete = new CsvRow();
                    //log.InfoFormat("starting delete. executioncycle: {0}", executioncycle);
                    Console.WriteLine("starting delete. executioncycle: {0}", executioncycle);
                    rowDelete.Start = DateTime.Now.ToString(dateTimeFormat);
                    stopWatch.Start();
                    ctx.Sql(delQuery.ToString()).Execute();
                    stopWatch.Stop();
                    rowDelete.End = DateTime.Now.ToString(dateTimeFormat);
                    //log.InfoFormat("finished delete. executioncycle: {0}", executioncycle);
                    Console.WriteLine("finished delete. executioncycle: {0}", executioncycle);
                    rowDelete.TimeTaken = stopWatch.Elapsed.TotalMilliseconds.ToString();
                    stopWatch.Reset();
                    deleteCollection.Add(rowDelete);


                    ctx.Sql("ALTER SEQUENCE test_table_id_seq RESTART WITH 1;").Execute();
                }
            }

            CreateCSVs(createCollection, retrieveCollection, updateCollection, deleteCollection);

            Console.WriteLine("Done");
            //  Console.ReadLine();
        }

        private static void Custom_mapper_using_datareader(TestTableRow arg1, IDataReader arg2)
        {
            arg1.Id = arg2.GetInt32("Id");
            arg1.Name = arg2.GetString("Name");
        }

        private static void CreateCSVs(List<CsvRow> createCollection, List<CsvRow> retrieveCollection,
            List<CsvRow> updateCollection,
            List<CsvRow> deleteCollection)
        {
            using (var sw = new StreamWriter(ConfigurationManager.AppSettings["csvLocationcreate"]))
            {
                var csv = new CsvWriter(sw);
                csv.Configuration.RegisterClassMap<CsvRowMap>();
                csv.WriteRecords(createCollection);
            }
            using (var sw = new StreamWriter(ConfigurationManager.AppSettings["csvLocationretrieve"]))
            {
                var csv = new CsvWriter(sw);

                csv.WriteRecords(retrieveCollection);
            }
            using (var sw = new StreamWriter(ConfigurationManager.AppSettings["csvLocationupdate"]))
            {
                var csv = new CsvWriter(sw);

                csv.WriteRecords(updateCollection);
            }
            using (var sw = new StreamWriter(ConfigurationManager.AppSettings["csvLocationdelete"]))
            {
                var csv = new CsvWriter(sw);

                csv.WriteRecords(deleteCollection);
            }
        }
    }

    public class TestTableRow
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
    }


    public class CsvRow
    {
        public string Start { get; set; }
        public string End { get; set; }
        public string TimeTaken { get; set; }
    }


    public sealed class CsvRowMap : CsvClassMap<CsvRow>
    {
        public CsvRowMap()
        {
            AutoMap();
        }
    }
}

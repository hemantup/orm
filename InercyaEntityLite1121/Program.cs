﻿/*
          -- this is sample db table which you are playing around with:
          -- Table: test_table

          -- DROP TABLE test_table;

          CREATE TABLE test_table
          (
            name text,
            id serial NOT NULL,
            CONSTRAINT test_table_pkey PRIMARY KEY (id)
          )
          WITH (
            OIDS=FALSE
          );
          ALTER TABLE test_table
            OWNER TO postgres;

*/
//Install-Package Npgsql -Version 3.1.10
//Install-Package EntityLite -Version 1.12.1
//delete DataLayer.tt
//delete EntityLite.ttinclude
//include DataLayer.cs  from https://hemantrohtak.blogspot.com/2017/02/generate-datalayer-for-i-nercya.html
//Install-Package CsvHelper -Version 2.16.3
//Install-Package log4net -Version 2.0.7
//Uninstall-Package log4net

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using CsvHelper;
using CsvHelper.Configuration;
using inercya.EntityLite;
using Npgsql;

//using log4net;
//using log4net.Config;

namespace InercyaEntityLite1121
{
    internal class InercyaEntityLite1121Test
    {
        private const string dateTimeFormat = "yyyy-MM-dd-HH-mm-ss-ffff";

        private static void Main(string[] args)
        {
            //XmlConfigurator.Configure();
            //var log = LogManager.GetLogger(typeof(InercyaEntityLite1121Test));
            var numberofrecords = Convert.ToInt32(args[0]);
            var numberofcycles = Convert.ToInt32(args[1]);
            // log.InfoFormat("(InercyaEntityLite1121Test) Number of records per cycle: {0}. Total Number of executioncycles: {1}", numberofrecords, numberofcycles);
            Console.WriteLine(
                "(InercyaEntityLite1121Test) Number of records per cycle: {0}. Total Number of executioncycles: {1}",
                numberofrecords,
                numberofcycles);
            //log.Info("started.");
            Console.WriteLine("started.");
            var createCollection = new List<CsvRow>();
            var retrieveCollection = new List<CsvRow>();
            var updateCollection = new List<CsvRow>();
            var deleteCollection = new List<CsvRow>();
            var stopWatch = new Stopwatch();
            for (var executioncycle = 1; executioncycle <= numberofcycles; executioncycle++)
            {
                //SELECT * FROM pg_stat_activity where application_name = 'InercyaEntityLite1121Test' and datname = 'test_InercyaEntityLite' and application_name != 'pgAdmin III - Query Tool' order by query_start desc;

                #region verify exit

                using (var ds = new GenratedDataService("PostgreeDbContext"))
                {
                    var initialRowCount = 0;

                    initialRowCount = ds.TestTableRowRepository.Query(Projection.BaseTable).GetCount();
                    // SELECT COUNT(*) FROM  "public"."test_table"

                    if (initialRowCount != 0)
                    {
                        //log.InfoFormat("Row count must have been 0 at the beginning of execution cycle {0}",executioncycle);
                        Console.WriteLine("Row count must have been 0 at the beginning of execution cycle {0}",
                            executioncycle);
                        Environment.Exit(-1);
                    }
                }

                #endregion

                using (var ds = new GenratedDataService("PostgreeDbContext"))
                {
                    var rowCreate = new CsvRow();
                    //  log.InfoFormat("starting create. executioncycle: {0}", executioncycle);
                    Console.WriteLine("starting create. executioncycle: {0}", executioncycle);
                    rowCreate.Start = DateTime.Now.ToString(dateTimeFormat);
                    for (var i = 1; i <= numberofrecords; i++)
                    {
                        var tb = new TestTableRow { Name = string.Format("{0}_Ex_Cycle:{1}", i, executioncycle) };
                        stopWatch.Start();
                        ds.TestTableRowRepository.Insert(tb);
                        //INSERT INTO  "public"."test_table"("name") VALUES ($1) RETURNING "id"
                        stopWatch.Stop();
                    }
                    rowCreate.End = DateTime.Now.ToString(dateTimeFormat);
                    // log.InfoFormat("finished creating. executioncycle: {0}", executioncycle);
                    Console.WriteLine("finished creating. executioncycle: {0}", executioncycle);
                    rowCreate.TimeTaken = stopWatch.Elapsed.TotalMilliseconds.ToString();
                    stopWatch.Reset();
                    createCollection.Add(rowCreate);
                }
                using (var ds = new GenratedDataService("PostgreeDbContext"))
                {
                    var rowRetrieve = new CsvRow();
                    // log.InfoFormat("starting retrieve. executioncycle: {0}", executioncycle);
                    Console.WriteLine("starting retrieve. executioncycle: {0}", executioncycle);
                    rowRetrieve.Start = DateTime.Now.ToString(dateTimeFormat);
                    stopWatch.Start();
                    var allrows = ds.TestTableRowRepository.Query(Projection.BaseTable).ToList();
                    //SELECT * FROM "public"."test_table" 
                    //as per pg_stat_activity
                    stopWatch.Stop();
                    rowRetrieve.End = DateTime.Now.ToString(dateTimeFormat);
                    // log.InfoFormat("finished retrieve. executioncycle: {0}", executioncycle);
                    Console.WriteLine("finished retrieve. executioncycle: {0}", executioncycle);
                    rowRetrieve.TimeTaken = stopWatch.Elapsed.TotalMilliseconds.ToString();
                    stopWatch.Reset();
                    retrieveCollection.Add(rowRetrieve);


                    //  log.InfoFormat("\tRetrieved {0} records after inserts.", allrows.Count);
                    Console.WriteLine("\tRetrieved {0} records after inserts.", allrows.Count);
                    var fstRecord = allrows.OrderBy(x => x.Id).First();
                    var lastRecord = allrows.OrderBy(x => x.Id).Last();
                    //  log.InfoFormat("\t\tfstRecord:: {0}||{1}", fstRecord.Id,fstRecord.Name);
                    Console.WriteLine("\t\tfstRecord:: {0}||{1}", fstRecord.Id, fstRecord.Name);
                    //  log.InfoFormat("\t\tlastRecord:: {0}||{1}", lastRecord.Id, lastRecord.Name);
                    Console.WriteLine("\t\tlastRecord:: {0}||{1}", lastRecord.Id, lastRecord.Name);

                    var rowUpdate = new CsvRow();
                    // log.InfoFormat("starting update. executioncycle: {0}", executioncycle);
                    Console.WriteLine("starting update. executioncycle: {0}", executioncycle);
                    rowUpdate.Start = DateTime.Now.ToString(dateTimeFormat);
                    foreach (var item in allrows)
                    {
                        item.Name = string.Format("{0}_{1}", item.Name, DateTime.Now.ToString(dateTimeFormat));
                        stopWatch.Start();
                        ds.TestTableRowRepository.Save(item);
                        //UPDATE "public"."test_table" SET "name" = $1 WHERE "id" = $2 AND ( "name" <> $1 OR "name" IS NULL AND $1 IS NOT NULL OR "name" IS NOT NULL AND $1 IS NULL )
                        // the second part of where simply confirms, update only if name is changed. 
                        stopWatch.Stop();
                    }
                    rowUpdate.End = DateTime.Now.ToString(dateTimeFormat);
                    // log.InfoFormat("finished update. executioncycle: {0}", executioncycle);
                    Console.WriteLine("finished update. executioncycle: {0}", executioncycle);
                    rowUpdate.TimeTaken = stopWatch.Elapsed.TotalMilliseconds.ToString();
                    stopWatch.Reset();
                    updateCollection.Add(rowUpdate);
                }

                using (var ds = new GenratedDataService("PostgreeDbContext"))
                {
                    var allrowsAgain = ds.TestTableRowRepository.Query(Projection.BaseTable).ToList();
                    //  log.InfoFormat("\tRetrieved {0} records after updates.", allrowsAgain.Count);
                    Console.WriteLine("\tRetrieved {0} records after updates.", allrowsAgain.Count);
                    var fstRecord = allrowsAgain.OrderBy(x => x.Id).First();
                    var lastRecord = allrowsAgain.OrderBy(x => x.Id).Last();
                    //  log.InfoFormat("\t\tfstRecord:: {0}||{1}", fstRecord.Id,fstRecord.Name);
                    Console.WriteLine("\t\tfstRecord:: {0}||{1}", fstRecord.Id, fstRecord.Name);
                    //  log.InfoFormat("\t\tlastRecord:: {0}||{1}", lastRecord.Id, lastRecord.Name);
                    Console.WriteLine("\t\tlastRecord:: {0}||{1}", lastRecord.Id, lastRecord.Name);

                    var rowDelete = new CsvRow();
                    // log.InfoFormat("starting delete. executioncycle: {0}", executioncycle);
                    Console.WriteLine("starting delete. executioncycle: {0}", executioncycle);
                    rowDelete.Start = DateTime.Now.ToString(dateTimeFormat);
                    foreach (var item in allrowsAgain)
                    {
                        stopWatch.Start();
                        ds.TestTableRowRepository.Delete(item);
                        //DELETE FROM "public"."test_table" WHERE "id" = $1
                        stopWatch.Stop();
                    }
                    rowDelete.End = DateTime.Now.ToString(dateTimeFormat);
                    // log.InfoFormat("finished delete. executioncycle: {0}", executioncycle);
                    Console.WriteLine("finished delete. executioncycle: {0}", executioncycle);
                    rowDelete.TimeTaken = stopWatch.Elapsed.TotalMilliseconds.ToString();
                    stopWatch.Reset();
                    deleteCollection.Add(rowDelete);
                }
                ResetSequence();
            }
            CreateCSVs(createCollection, retrieveCollection, updateCollection, deleteCollection);

            Console.WriteLine("Done");
            // Console.ReadLine();
        }

        private static void ResetSequence()
        {
            var cString = ConfigurationManager.ConnectionStrings["PostgreeDbContext"].ConnectionString;
            var conn = new NpgsqlConnection(cString);
            conn.Open();
            new NpgsqlCommand("ALTER SEQUENCE test_table_id_seq RESTART WITH 1;", conn).ExecuteNonQuery();
            conn.Close();
        }

        private static void CreateCSVs(List<CsvRow> createCollection, List<CsvRow> retrieveCollection,
            List<CsvRow> updateCollection,
            List<CsvRow> deleteCollection)
        {
            using (var sw = new StreamWriter(ConfigurationManager.AppSettings["csvLocationcreate"]))
            {
                var csv = new CsvWriter(sw);
                csv.Configuration.RegisterClassMap<CsvRowMap>();
                csv.WriteRecords(createCollection);
            }
            using (var sw = new StreamWriter(ConfigurationManager.AppSettings["csvLocationretrieve"]))
            {
                var csv = new CsvWriter(sw);

                csv.WriteRecords(retrieveCollection);
            }
            using (var sw = new StreamWriter(ConfigurationManager.AppSettings["csvLocationupdate"]))
            {
                var csv = new CsvWriter(sw);

                csv.WriteRecords(updateCollection);
            }
            using (var sw = new StreamWriter(ConfigurationManager.AppSettings["csvLocationdelete"]))
            {
                var csv = new CsvWriter(sw);

                csv.WriteRecords(deleteCollection);
            }
        }
    }

    public class CsvRow
    {
        public string Start { get; set; }
        public string End { get; set; }
        public string TimeTaken { get; set; }
    }


    public sealed class CsvRowMap : CsvClassMap<CsvRow>
    {
        public CsvRowMap()
        {
            AutoMap();
        }
    }
}

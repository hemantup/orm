﻿
using System;
using System.Linq;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Runtime.Serialization;
using System.ComponentModel;
using inercya.EntityLite;	
using inercya.EntityLite.Extensions;		

namespace InercyaEntityLite1121
{
	[Serializable]
	[DataContract]
	[SqlEntity(BaseTableName="test_table")]
	public partial class TestTableRow
	{
		private String _name;
		[DataMember]
		[SqlField(DbType.String, -1, ColumnName ="name", BaseColumnName ="name", BaseTableName = "test_table" )]
		public String Name 
		{ 
		    get { return _name; } 
			set 
			{
			    _name = value;
			}
        }

		private Int32 _id;
		[DataMember]
		[SqlField(DbType.Int32, 4, IsKey=true, IsAutoincrement=true, ColumnName ="id", BaseColumnName ="id", BaseTableName = "test_table" )]
		public Int32 Id 
		{ 
		    get { return _id; } 
			set 
			{
			    _id = value;
			}
        }


	}

	public partial class TestTableRowRepository : Repository<TestTableRow> 
	{
		public TestTableRowRepository(DataService DataService) : base(DataService)
		{
		}

		public new GenratedDataService  DataService  
		{
			get { return (GenratedDataService) base.DataService; }
			set { base.DataService = value; }
		}

		public TestTableRow Get(string projectionName, System.Int32 id)
		{
			return ((IRepository<TestTableRow>)this).Get(projectionName, id, FetchMode.UseIdentityMap);
		}

		public TestTableRow Get(string projectionName, System.Int32 id, FetchMode fetchMode = FetchMode.UseIdentityMap)
		{
			return ((IRepository<TestTableRow>)this).Get(projectionName, id, fetchMode);
		}

		public TestTableRow Get(Projection projection, System.Int32 id)
		{
			return ((IRepository<TestTableRow>)this).Get(projection, id, FetchMode.UseIdentityMap);
		}

		public TestTableRow Get(Projection projection, System.Int32 id, FetchMode fetchMode = FetchMode.UseIdentityMap)
		{
			return ((IRepository<TestTableRow>)this).Get(projection, id, fetchMode);
		}

		public TestTableRow Get(string projectionName, System.Int32 id, params string[] fields)
		{
			return ((IRepository<TestTableRow>)this).Get(projectionName, id, fields);
		}

		public TestTableRow Get(Projection projection, System.Int32 id, params string[] fields)
		{
			return ((IRepository<TestTableRow>)this).Get(projection, id, fields);
		}

		public bool Delete(System.Int32 id)
		{
			var entity = new TestTableRow { Id = id };
			return this.Delete(entity);
		}
	}

	public static partial class TestTableRowFields
	{
		public const string Name = "Name";
		public const string Id = "Id";
	}

}

namespace InercyaEntityLite1121
{
	public partial class GenratedDataService : DataService
	{
		partial void OnCreated();

		private void Init()
		{
			EntityNameToEntityViewTransform = TextTransform.None;
			EntityLiteProvider.DefaultSchema = "public";
			OnCreated();
		}

        public GenratedDataService() : base("PostgreeDbContext")
        {
			Init();
        }

        public GenratedDataService(string connectionStringName) : base(connectionStringName)
        {
			Init();
        }

        public GenratedDataService(string connectionString, string providerName) : base(connectionString, providerName)
        {
			Init();
        }

		private InercyaEntityLite1121.TestTableRowRepository _TestTableRowRepository;
		public InercyaEntityLite1121.TestTableRowRepository TestTableRowRepository
		{
			get 
			{
				if ( _TestTableRowRepository == null)
				{
					_TestTableRowRepository = new InercyaEntityLite1121.TestTableRowRepository(this);
				}
				return _TestTableRowRepository;
			}
		}
	}
}
